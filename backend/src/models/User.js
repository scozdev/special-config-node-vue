var mongoose = require("mongoose");

var UserSchema = new mongoose.Schema(
  {
    username: { type: String, unique: true, required: true, index: true },
  },
  { timestamps: true }
);

mongoose.model("User", UserSchema);
