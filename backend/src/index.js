const http = require("http");
const https = require("https");
const express = require("express");

const cors = require("cors");
const bodyParser = require("body-parser");

const routes = require("./routes");

const app = express();

app.use(cors());

// Parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// app.use(express.static(path.join(__dirname, 'dist')));

// api routes
app.use("/", routes);

app.get("/", (req, res) => {
  res.send("Now using https..");
});

const httpServer = http.createServer(app);
const httpsServer = https.createServer(app);

httpServer.listen(8080, () => {
  console.log(`Server listening port . ${8080}`);
});
httpsServer.listen(8443, () => {
  console.log(`Server listening port . ${8443}`);
});

module.exports = app;
