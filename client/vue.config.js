//const path = require('path');
const ExtractTextPlugin = require("mini-css-extract-plugin");

module.exports = {
  publicPath: process.env.NODE_ENV === "production" ? "/client/" : "/",
  // css: { sourceMap: false },
  configureWebpack: {
    // devtool: "source-map",
    devtool: "eval-source-map",
    // No need for splitting
    optimization: {
      splitChunks: false,
    },
  },
  chainWebpack: (config) => {
    // Disable splitChunks plugin, all the code goes into one bundle.

    config.optimization.splitChunks().clear();

    config.output.filename("js/[name].js");

    config.plugin("extract-css").use(ExtractTextPlugin, [
      {
        filename: "css/[name].css",
        allChunks: true,
      },
    ]);
  },
  // css: {
  //   extract: false,
  // },
};
